# Command Pattern #

## Context ##

Command pattern design is used to encapsulate a request as an object. 
  
There are four term that associated wth command pattern.Command, Reciever, Invoker, Client.

![Command-2x.png](https://bitbucket.org/repo/KgoKMx/images/4285973376-Command-2x.png)

## Motivation ##

We don't want to mix the logic (working part) code in the UI.

## Example ##


```
#!java
	public interface ActionListenerCommand {
	    public void execute();
	}

	public class Document {
	   public void Open(){
	       System.out.println('Document Opened');
	   }
	   public void Save(){
	       System.out.println('Document Saved');
	   }
	}
	public class ActionOpen implements ActionListenerCommand {
	    private Document adoc;
	 
	    public ActionOpen(Document doc) {
	        this.adoc = doc;
	    }
	    @Override
	    public void execute() {
	        adoc.Open();
	    }
	}
	public class MenuOptions {
	    private ActionListenerCommand openCommand;
	    private ActionListenerCommand saveCommand;
	 
	    public MenuOptions(ActionListenerCommand open, ActionListenerCommand save) {
	        this.openCommand = open;
	        this.saveCommand = save;
	    }
	    public void clickOpen(){
	        openCommand.execute();
	    }
            public void clickSave(){
	        saveCommand.execute();
	    }
	}
	public class Client {
	    public static void main(String[] args) {
	        Document doc = new Document();
	        ActionListenerCommand clickOpen = new ActionOpen(doc);
	        ActionListenerCommand clickSave = new ActionSave(doc);
	        MenuOptions menu = new MenuOptions(clickOpen, clickSave);
	        menu.clickOpen();
	        menu.clickSave();
	    }
	 
	}
```

## Example test ##

You are going to develop the racing game.Write the code so its can acceleration brake turn left and right and use boost using command pattern.


## Pro ##
* The object that invokes a command isn’t coupled to the object that performs the command.
* All command have same interface.
* The command can be reused,passed as parameter, etc.